import axios from 'axios'  // 导入axios库
import { showToast } from 'vant'  // 导入toast弹窗组件来显示消息提示

const instance = axios.create({
  baseURL: '/api',  // 设置请求的基础URL为/api
  method: 'POST',  // 设置默认请求方法为POST
  timeout: 7000  // 设置请求的超时时间为7000毫秒（7秒）
})

instance.interceptors.request.use(
  (config) => {
    config.data = {
      ...config.data
    }
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  (response) => {
    const { data, code, msg } = response.data

    if (code === 1) {
      return data
    } else {
      showToast(msg || `接口异常`)  // 显示接口返回的错误消息或默认的"接口异常"提示
      return Promise.reject(data)
    }
  },
  (error) => {
    if (error.response) {
      return Promise.reject(error.response.data)
    } else {
      return Promise.reject(error)
    }
  }
)

export default instance  // 导出axios实例

