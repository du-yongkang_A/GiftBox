import { defineStore } from 'pinia' // 导入Pinia库的defineStore函数
import { ref } from 'vue' // 导入Vue库的ref函数
import { baseURL, integralIndex } from '../api'  // 导入自定义的api模块中的baseURL和integralIndex

export const useIntegrallist = defineStore(
  'integralList',  // 定义Store的名称为'integralList'
  () => {
    const jobs = ref([])  // 创建一个响应式的jobs数组
    const loading = ref(true)  // 创建一个响应式的loading布尔值，默认为true
    const curJob = ref({})  // 创建一个响应式的curJob对象，默认为空对象

    async function getCur(item) {
      curJob.value = item  // 将传入的item赋值给curJob
    }

    const curGiftJob = ref({})  // 创建一个响应式的curGiftJob对象，默认为空对象

    async function getCurGift(item) {
      curGiftJob.value = item  // 将传入的item赋值给curGiftJob
    }

    const curClock = ref({})  // 创建一个响应式的curClock对象，默认为空对象

    async function getCurClock(item) {
      curClock.value = item  // 将传入的item赋值给curClock
    }

    function render() {
      ;(async function () {
        try {
          loading.value = true  // 设置loading为true
          const {
            data: { date }
          } = await integralIndex()  // 调用integralIndex函数获取数据，并将结果解构为date变量

          // 对获取的数据进行处理，将image路径添加baseURL前缀，并更新jobs数组
          jobs.value = date.map((item) => {
            item.image = baseURL + item.image
            return item
          })

          loading.value = false  // 设置loading为false
        } catch (error) {
          loading.value = false  // 设置loading为false
        }
      })()
    }
    render()  // 第一次加载组件时触发render函数，获取数据并更新状态

    return {
      jobs,  // 导出jobs数组
      loading,  // 导出loading布尔值
      curJob,  // 导出curJob对象
      render,  // 导出render函数
      getCur,  // 导出getCur函数
      curGiftJob,  // 导出curGiftJob对象
      getCurGift,  // 导出getCurGift函数
      curClock,  // 导出curClock对象
      getCurClock  // 导出getCurClock函数
    }
  },
  {
    persist: {
      storage: sessionStorage,  // 设置持久化存储为sessionStorage
      paths: ['curJob', 'curGiftJob']  // 设置需要持久化的状态属性
    }
  }
)
