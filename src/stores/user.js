import { baseURL, loginIndex, assignmentIntegralList } from '@/api'  // 导入自定义的api模块中的baseURL、loginIndex和assignmentIntegralList
import { defineStore } from 'pinia'  // 导入Pinia库的defineStore函数
import { computed, reactive, ref } from 'vue'  // 导入Vue库的computed、reactive和ref函数

export const useUserInfoStore = defineStore(
  'user',  // 定义Store的名称为"user"
  () => {
    const person = reactive({})  // 创建一个响应式的person对象
    const loading = computed(() => !person.id)  // 创建一个计算属性loading，根据person.id判断数据是否加载完成
    const student = ref([])  // 创建一个响应式的student数组
    const curStudentIndex = ref(0)  // 创建一个响应式的curStudentIndex索引，默认为0
    const curStudent = computed(() => student.value[curStudentIndex.value])  // 创建一个计算属性curStudent，根据curStudentIndex获取当前学生信息

    const thisCount = reactive({  // 创建一个响应式的thisCount对象，用于记录统计信息
      add: 0,
      del: 0,
      cont: 0,
      list: []
    })

    function setCurStatus(res) {
      student.value = [{ ...res, avatar: baseURL + res.avatar }]  // 将传入的res对象添加avatar属性，并将其赋值给student数组
    }

    async function getUserInfo() {
      const res = await loginIndex()  // 调用loginIndex函数获取用户信息
      Object.assign(person, res.person)  // 将res.person的属性复制到person对象中

      const ress = await assignmentIntegralList({ user_id: 1 })  // 调用assignmentIntegralList函数获取统计信息
      Object.assign(thisCount, {
        ...ress,
      })
    }
    getUserInfo()  // 在初始化时调用getUserInfo函数获取数据

    function setCurStudentIndex(index) {
      curStudentIndex.value = index  // 设置curStudentIndex的值为传入的index
    }

    return {
      setCurStatus,
      thisCount,
      person,
      student,
      loading,
      curStudent,
      curStudentIndex,
      setCurStudentIndex,
      getUserInfo,
      classFormat: reactive({
        1: `小班`,
        2: `中班`,
        3: `大班`
      })
    }
  },
  {
    persist: {
      storage: sessionStorage,  // 设置持久化存储为sessionStorage
      paths: ['person', 'student', 'curStudentIndex']  // 设置需要持久化的状态属性
    }
  }
)
