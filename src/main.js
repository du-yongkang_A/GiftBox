import './assets/main.css'

import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import Vant, { ConfigProvider, Lazyload,PullRefresh,FloatingBubble } from 'vant'
import 'vant/es/dialog/style'
import 'vant/es/image-preview/style'
import 'vant/es/notify/style'
import 'vant/es/toast/style'
import 'vant/lib/index.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
const app = createApp(App)

app
  .use(pinia)
  .use(Vant)
  .use(ConfigProvider)
  .use(Lazyload, { lazyComponent: true })
  .use(router)
  .use(PullRefresh)
  .use(FloatingBubble)
  .mount('#app')
