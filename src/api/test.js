function init(key, value) {
  const key_ = key
  let defaultValue
  defaultValue = get() ? get() : value
  function set(newValue) {
    localStorage.setItem(key_, JSON.stringify(newValue))
  }
  function get() {
    const storage = localStorage.getItem(key_) || ``
    return storage ? (JSON.parse(storage) !== value ? value : JSON.parse(storage)) : defaultValue
  }
  set(defaultValue)
  return { set, get }
}

const studentProfile = init('studentProfile', [
  {
    id: 1,
    name: `刘小浩`,
    grade: `小班`,
    expirae: `2022-04-30`,
    default: 1
  },
  {
    id: 2,
    name: `刘小浩1`,
    grade: `小班`,
    expirae: `2022-04-29`,
    default: 0
  },
  {
    id: 3,
    name: `刘小浩2`,
    grade: `小班`,
    expirae: `2022-04-25`,
    default: 0
  }
])

const userInfo = init('userInfo', {
  //用户信息
  id: 1,
  nickname: `森眸暖光`,
  avatar: `https://gw.alicdn.com/i3/710600684/O1CN018zVrPd1GvJlPH3RWu_!!710600684.jpg_Q75.jpg_.webp`,
  integral: 23000,
  profile: studentProfile.get().find((item) => item.default)
})

export async function getUserInfo() {
  return new Promise((resolve) => setTimeout(() => resolve(userInfo.get()), 700))
}

export async function getStudentProfile() {
  return new Promise((resolve) => {
    setTimeout(() => resolve(studentProfile.get()), 700)
  })
}
export async function getCounseling() {
  //咨询列表
  const counseling = init('counseling', [
    {
      id: 1,
      title: `“双减”落地 新学期老师应该如何布置作业新学期老师应该如何布置作业新学期老师应该如何布置作业`,
      img: `https://img95.699pic.com/photo/50062/8783.jpg_wh300.jpg`,
      release_time: `2022-02-20`,
      num: 275,
      content: `
    <p>
    <p>“双减”落地 新学期老师应该如何布置作业</p>
    <img src="https://pix.veryjack.com/i/2023/04/04/fsxnkv.webp" style="width:100%"/>
    <p>“双减”落地 新学期老师应该如何布置作业</p>
    </p>`
    },
    {
      id: 2,
      title: `“双减”落地 新学期老师应该如何布置作业`,
      img: `https://img95.699pic.com/photo/50062/8783.jpg_wh300.jpg`,
      release_time: `2022-02-20`,
      num: 275,
      content: `
    <p>
    <img src="https://pix.veryjack.com/i/2023/04/04/fsxnkv.webp" style="width:100%"/>
    <p>“双减”落地 新学期老师应该如何布置作业</p>
    </p>`
    }
  ])
  return new Promise((resolve) => {
    setTimeout(() => resolve(counseling.get()), 700)
  })
}

export async function getAddress() {
  //地址
  const address = init('address', [
    {
      id: 1,
      name: `杜永康`,
      phone: '14578965127',
      district: `安徽省合肥市蜀山区`,
      address: `蓝光禹州城1栋1单元101室`,
      status: 0
    },
    {
      id: 2,
      name: `杜永康1`,
      phone: '15632458714',
      district: `安徽省合肥市蜀山区`,
      address: `蓝光禹州城1栋1单元101室`,
      status: 1
    }
  ])
  return new Promise((resolve) => {
    setTimeout(() => resolve(address.get()), 700)
  })
}

export async function getClock() {
  //打卡
  const clock = init('clock', [
    {
      id: 1,
      title: `任务标题`,
      assignment: `任务内容`,
      date: '2022-02-20',
      status: 0,
      images: ``
    }
  ])
  return new Promise((resolve) => {
    setTimeout(() => resolve(clock.get()), 700)
  })
}

export async function getGiftBox() {
  //礼盒
  const giftBox = init('giftBox', [
    {
      id: 1,
      image: `https://img95.699pic.com/element/40250/5909.png_300.png`,
      name: `这里是标题内容这里...`,
      details: '这里是标题内容这里是标题',
      money: 0,
      // user_id,
      class: 1, //礼盒分类 1小班,2中班,3大班
      package: null,
      number: 10
      // status,
    },
    {
      id: 2,
      image: `https://bpic.588ku.com/element_pic/23/04/25/fe9f8796f403c7a4db9caba8f3eb9e6f.png!/fw/329/quality/90/unsharp/true/compress/true`,
      name: `这里是标题内容这里...`,
      details: '这里是标题内容这里是标题',
      money: 0,
      class: 1,
      package: null,
      number: 10
    },
    {
      id: 3,
      image: `https://pic.616pic.com/ys_img/00/04/12/lIyfg7nzcx.jpg`,
      name: `这里是标题内容这里...`,
      details: '这里是标题内容这里是标题',
      money: 0,
      class: 1,
      package: null,
      number: 10
    },
    {
      id: 4,
      image: `https://img95.699pic.com/element/40187/0108.png_300.png`,
      name: `这里是标题内容这里...`,
      details: '这里是标题内容这里是标题',
      money: 0,
      class: 1,
      package: null,
      number: 10
    },
    {
      id: 5,
      image: `https://bpic.588ku.com/element_pic/23/04/25/fe9f8796f403c7a4db9caba8f3eb9e6f.png!/fw/329/quality/90/unsharp/true/compress/true`,
      name: `这里是标题内容这里...`,
      details: '这里是标题内容这里是标题',
      money: 0,
      class: 1,
      package: null,
      number: 10
    }
  ])
  return new Promise((resolve) => {
    setTimeout(() => resolve(giftBox.get()), 700)
  })
}
