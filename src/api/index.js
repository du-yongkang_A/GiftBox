import request from '@/utils/request'
import axios from 'axios'
export const baseURL = `http://wxy.jingyi.icu`
/*首页*/ export const homeIndex = async (data) => request({ url: '/app/home/index', data })
/*礼盒列表*/ export const homeGiftList = async (data) =>
  request({ url: '/app/home/giftList', data })
/*获取礼盒商品订单*/ export const giftBoxOrder = async (data) =>
  request({ url: '/app/Giftbox/order', data })
/*礼盒商品订单*/ export const giftBoxPayment = async (data) =>
  request({ url: '/app/Giftbox/payment', data })
/*商品订单列表*/ export const giftBoxLists = async (data) =>
  request({ url: '/app/Giftbox/lists', data })
/*积分商品*/ export const integralIndex = async (data) =>
  axios({ url: '/api/app/integral/index', method: 'POST', data })
/*积分商品收藏*/ export const integralCollect = async (data) =>
  request({ url: '/app/integral/collect', data })
/*积分商品订单详情*/ export const integralOrder = async (data) =>
  request({ url: '/app/integral/order', data })
/*积分商品兑换*/ export const integralPayment = async (data) =>
  request({ url: '/app/integral/payment', data })
/*积分商品完成*/ export const integralSucce = async (data) =>
  request({ url: '/app/integral/succe', data })
/*礼盒商品完成*/ export const giftBoxSucce = async (data) =>
  request({ url: '/app/Giftbox/succe', data })

/*礼盒商品订单取消*/ export const quxiaointegral = async (data) =>
request({ url: '/app/integral/orderdel', data })

  
/*积分商品订单取消*/ export const quxiaoorderdel = async (data) =>
request({ url: '/app/Giftbox/orderdel', data })

/*积分商品订单列表*/ export const integralLists = async (data) =>
  request({ url: '/app/integral/lists', data })
/*我的收藏列表*/ export const homeListss = async (data) =>
  request({ url: '/app/home/listss', data })
/*删除收藏*/ export const homeDel = async (data) => request({ url: '/app/home/del', data })
/*个人中心*/ export const loginIndex = async (data) => request({ url: '/app/login/index', data })
/*学员删除*/ export const loginDel = async (data) => request({ url: '/app/login/del', data })
/*学员编辑*/ export const loginEdit = async (data) => request({ url: '/app/login/edit', data })
/*地址列表*/ export const addressIndex = async (data) =>
  request({ url: '/app/address/index', data })
/*地址添加*/ export const addressAdd = async (data) => request({ url: '/app/address/add', data })
/*地址编辑*/ export const addressEdit = async (data) => request({ url: '/app/address/edit', data })
/*地址删除*/ export const addressDel = async (data) => request({ url: '/app/address/del', data })
/*打卡列表*/ export const assignmentIndex = async (data) =>
  request({ url: '/app/assignment/index', data })
/*打卡成功*/ export const assignmentStart = async (data) =>
  request({ url: '/app/assignment/start', data })

/*打卡记录*/ export const assignmentClockList = async (data) =>
  request({ url: '/app/assignment/clockList', data })
/*积分记录*/ export const assignmentIntegralList = async (data) =>
  request({ url: '/app/assignment/integralList', data })
/*咨询列表*/ export const consultingIndex = async (data) =>
  request({ url: '/app/consulting/index', data })
/*上传*/ export const upload = async (data) =>
  axios({ url: '/api/uploads/upload', method: 'POST', data })
/*登录*/export const login = async (data) => request({ url: '/app/login/login', data })