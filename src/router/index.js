import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Login',
      component: () => import('@/views/Login/login.vue'),
      meta: { title: '登录',hideHeader:true }
    },

    {
      path: '/tabBarView',
      name: 'TabBarView',
      redirect:'/home',
      component: () => import('@/views/TabBarView/index.vue'),
      children: [
        {
          path: '/home',
          name: 'Home',
          component: () => import('@/views/TabBarView/Home.vue'),
          meta: { title: '首页', keepAlive: true }
        },
        {
          path: '/clock',
          name: 'Clock',
          component: () => import('@/views/TabBarView/Clock.vue'),
          meta: { title: '打卡' }
        },
        {
          path: '/integralMall',
          name: 'IntegralMall',
          component: () => import('@/views/TabBarView/IntegralMall.vue'),
          meta: { title: '积分商城' }
        },
        {
          path: '/personalCenter',
          name: 'PersonalCenter',
          component: () => import('@/views/TabBarView/PersonalCenter.vue'),
          meta: { title: '个人中心' }
        }
      ]
    },

    
    {
      path: '/myStar',
      name: 'MyStar',
      component: () => import('@/views/MyStar.vue'),
      meta: { title: '我的收藏' }
    },
    {
      path: '/studentProfile',
      name: 'StudentProfile',
      component: () => import('@/views/StudentProfile.vue'),
      meta: { title: '学员档案' }
    },
    {
      path: '/editStudentProfile',
      name: 'EditStudentProfile',
      component: () => import('@/views/EditStudentProfile.vue'),
      meta: { title: '编辑档案' }
    },
    {
      path: '/myIntegral',
      name: 'MyIntegral',
      component: () => import('@/views/MyIntegral.vue'),
      meta: { title: '积分记录' }
    },
    {
      path: '/punchList',
      name: 'PunchList',
      component: () => import('@/views/PunchList.vue'),
      meta: { title: '打卡记录' }
    },
    {
      path: '/giftBoxList',
      name: 'GiftBoxList',
      component: () => import('@/views/GiftBoxList.vue'),
      meta: { title: '礼盒列表' }
    },
    {
      path: '/giftBoxDetails',
      name: 'GiftBoxDetails',
      component: () => import('@/views/GiftBoxDetails.vue'),
      meta: { title: '礼盒详情' }
    },
    {
      path: '/giftOrders',
      name: 'GiftOrders',
      component: () => import('@/views/GiftOrders.vue'),
      meta: { title: '礼盒订单' }
    },
    {
      path: '/integralOrder',
      name: 'IntegralOrder',
      component: () => import('@/views/IntegralOrder.vue'),
      meta: { title: '积分订单' }
    },
    {
      path: '/addressManagement',
      name: 'AddressManagement',
      component: () => import('@/views/AddressManagement.vue'),
      meta: { title: '地址管理' }
    },
    {
      path: '/editAddress',
      name: 'EditAddress',
      component: () => import('@/views/EditAddress.vue'),
      meta: { title: '编辑地址' }
    },
    {
      path: '/addAddress',
      name: 'AddAddress',
      component: () => import('@/views/AddAddress.vue'),
      meta: { title: '添加地址' }
    },
    {
      path: '/confirmationOfPayment',
      name: 'ConfirmationOfPayment',
      component: () => import('@/views/ConfirmationOfPayment.vue'),
      meta: { title: '确认支付' }
    },
    {
      path: '/querenOrder',
      name: 'QuerenOrder',
      component: () => import('@/views/QuerenOrder.vue'),
      meta: { title: '确认订单' }
    },
    {
      path: '/couponCenter',
      name: 'CouponCenter',
      component: () => import('@/views/CouponCenter.vue'),
      meta: { title: '优惠券中心' }
    },
    {
      path: '/clockDetail',
      name: 'ClockDetail',
      component: () => import('@/views/ClockDetail.vue'),
      meta: { title: '打卡详情页' }
    },
    {
      path: '/goodsDetail',
      name: 'GoodsDetail',
      component: () => import('@/views/GoodsDetail.vue'),
      meta: { title: '商品详情' }
    },
    {
      path: '/counselingCenter',
      name: 'CounselingCenter',
      component: () => import('@/views/CounselingCenter.vue'),
      meta: { title: '咨询中心' }
    },
    {
      path: '/consultationDetails',
      name: 'ConsultationDetails',
      component: () => import('@/views/ConsultationDetails.vue'),
      meta: { title: '咨询详情' }
    }
  ]
})

export default router
